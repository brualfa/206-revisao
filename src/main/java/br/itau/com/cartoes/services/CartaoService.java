package br.itau.com.cartoes.services;

import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.itau.com.cartoes.exceptions.ValidacaoException;
import br.itau.com.cartoes.models.Cartao;
import br.itau.com.cartoes.models.Cliente;
import br.itau.com.cartoes.repositories.CartaoRepository;

@Service
public class CartaoService {
  @Autowired
  private CartaoRepository cartaoRepository;
  @Autowired
  private ClienteService clienteService;
  
  public Cartao criar(Cartao cartao) {
	Cliente cliente = cartao.getCliente();  
	  
    Optional<Cliente> optional = clienteService.buscar(cliente.getId());
    
    if(!optional.isPresent()) {
       throw new ValidacaoException("cliente", "Cliente não encontrado");
    }
    
    cartao.setNumero(gerarNumero());
    cartao.setAtivo(false);
    
    return cartaoRepository.save(cartao);
  }
  
  public Optional<Cartao> buscar(String numero){
    return cartaoRepository.findById(numero);
  }
  
  private String gerarNumero() {
    Random random = new Random();
    String numero = "";
    
    for(int i = 0; i < 16; i ++) {
       numero += random.nextInt(9);
    }
    
    if(cartaoRepository.findById(numero).isPresent()) {
      return gerarNumero();
    }
    
    return numero;
  }
  
  public void ativar(String numero) {
    Optional<Cartao> cartaoOptional = cartaoRepository.findById(numero);
    
    if(!cartaoOptional.isPresent()) {
      throw new ValidacaoException("numero", "Cartão não encontrado.");
    }
    
    Cartao cartao = cartaoOptional.get();
    
    if(cartao.getAtivo()) {
      throw new ValidacaoException("numero", "Cartão já está ativo.");
    }
    
    cartao.setAtivo(true);
    cartaoRepository.save(cartao);
  }
}
