package br.itau.com.cartoes.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.itau.com.cartoes.models.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
  Optional<Cliente> findByCpf(String cpf);
}
