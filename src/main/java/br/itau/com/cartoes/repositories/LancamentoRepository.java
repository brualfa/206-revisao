package br.itau.com.cartoes.repositories;

import org.springframework.data.repository.CrudRepository;

import br.itau.com.cartoes.models.Lancamento;

public interface LancamentoRepository extends CrudRepository<Lancamento, Integer>{
  Iterable<Lancamento> findAllByCartao_numero(String numero);
}
