package br.itau.com.cartoes.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.itau.com.cartoes.models.Bandeira;
import br.itau.com.cartoes.models.Cartao;
import br.itau.com.cartoes.models.Cliente;
import br.itau.com.cartoes.services.CartaoService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CartaoController.class)
public class CartaoControllerTest {
  @Autowired
  private MockMvc mockMvc;
  
  @MockBean
  private CartaoService cartaoService;
  
  private ObjectMapper mapper = new ObjectMapper();
  
  private Bandeira bandeira = Bandeira.MASTERCARD;
  
  @Test
  @WithMockUser(username = "1")
  public void deveCriarUmCartao() throws Exception {
    HashMap<String, Object> cartao = new HashMap<>();
    cartao.put("bandeira", bandeira);
    cartao.put("ativo", false);
    cartao.put("senha", "1234");
    
    String cartaoJson = mapper.writeValueAsString(cartao);
    
    when(cartaoService.criar(any(Cartao.class))).then(answer -> answer.getArgument(0));
    
    mockMvc.perform(post("/cartao")
              .contentType(MediaType.APPLICATION_JSON_UTF8)
              .content(cartaoJson))
            .andExpect(status().isCreated())
            .andExpect(content().string(containsString(bandeira.toString())))
            .andExpect(content().string(containsString("false")));
  }
  
  @Test
  @WithMockUser(username = "1")
  public void deveValidarDadosDoCartao() throws Exception {
    HashMap<String, Object> cartao = new HashMap<>();
    cartao.put("senha", "1234124213421341");
    
    String cartaoJson = mapper.writeValueAsString(cartao);
    
    mockMvc.perform(post("/cartao")
              .contentType(MediaType.APPLICATION_JSON_UTF8)
              .content(cartaoJson))
            .andExpect(status().isUnprocessableEntity())
            .andExpect(content().string(containsString("senha")))
            .andExpect(content().string(containsString("bandeira")));
  }
  
  @Test
  @WithMockUser(username = "1")
  public void deveAtivarCartao() throws Exception {
    String numero = "1234123412341234";
    
    Cliente cliente = new Cliente();
    cliente.setId(1);
    
    Cartao cartao = new Cartao();
    cartao.setNumero(numero);
    cartao.setCliente(cliente);
    
    when(cartaoService.buscar(numero)).thenReturn(Optional.of(cartao));

    mockMvc.perform(patch("/cartao/" + numero + "/ativar"))
      .andExpect(status().isOk());
  }
  
  @Test
  @WithMockUser(username = "1")
  public void deveRetornarErroAoAtivarCartaoInexistente() throws Exception {
    String numero = "1234123412341234";
    
    when(cartaoService.buscar(numero)).thenReturn(Optional.empty());

    mockMvc.perform(patch("/cartao/" + numero + "/ativar"))
      .andExpect(status().isNotFound());
  }
}
