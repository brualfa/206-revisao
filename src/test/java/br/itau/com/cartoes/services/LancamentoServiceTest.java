package br.itau.com.cartoes.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.itau.com.cartoes.exceptions.ValidacaoException;
import br.itau.com.cartoes.models.Cartao;
import br.itau.com.cartoes.models.Lancamento;
import br.itau.com.cartoes.repositories.LancamentoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = LancamentoService.class)
public class LancamentoServiceTest {
  @Autowired
  private LancamentoService sujeito;
  
  @MockBean
  private LancamentoRepository lancamentoRepository;
  @MockBean
  private CartaoService cartaoService;
  
  private Cartao cartao;
  private Lancamento lancamento;
  private String numeroCartao = "1234156718901230";
  private BigDecimal valor = BigDecimal.valueOf(10.0);
  private String descricao = "uma descricao";
  
  @Before
  public void preparar() {
    cartao = new Cartao();
    cartao.setNumero(numeroCartao);
    
    lancamento = new Lancamento();
    lancamento.setCartao(cartao);
    lancamento.setValor(valor);
    lancamento.setDescricao(descricao);
  }
  
  @Test
  public void deveCriarUmLancamento() {
	cartao.setAtivo(true);
	  
    when(cartaoService.buscar(numeroCartao)).thenReturn(Optional.of(cartao));
    when(lancamentoRepository.save(any(Lancamento.class))).then(answer -> answer.getArgument(0));
    
    Lancamento lancamentoCriado = sujeito.criar(lancamento);
    
    assertNotNull(lancamentoCriado.getHorario());
    assertEquals(cartao, lancamentoCriado.getCartao());
    assertEquals(valor, lancamentoCriado.getValor());
    assertEquals(descricao, lancamentoCriado.getDescricao());
  }
  
  @Test(expected = ValidacaoException.class)
  public void deveLancarExcessaoAoCriarLancamentoDeCartaoInativo() {
	cartao.setAtivo(false);
	
    when(cartaoService.buscar(numeroCartao)).thenReturn(Optional.of(cartao));
    when(lancamentoRepository.save(any(Lancamento.class))).then(answer -> answer.getArgument(0));
    
    Lancamento lancamentoCriado = sujeito.criar(lancamento);
    
    assertNotNull(lancamentoCriado.getHorario());
    assertEquals(cartao, lancamentoCriado.getCartao());
    assertEquals(valor, lancamentoCriado.getValor());
    assertEquals(descricao, lancamentoCriado.getDescricao());
  }
  
  @Test(expected = ValidacaoException.class)
  public void deveLancarExcessaoAoCriarLancamentoDeCartaoInexistente() {
    when(cartaoService.buscar(numeroCartao)).thenReturn(Optional.empty());
    
    sujeito.criar(lancamento);
  }
  
  @Test
  public void deveBuscarTodosOsLancamentosDeUmCartao() {
    when(lancamentoRepository.findAllByCartao_numero(numeroCartao)).thenReturn(Lists.list(lancamento));
    
    Iterable<Lancamento> lancamentosIterable = sujeito.buscarTodasPorNumeroDoCartao(numeroCartao);
    List<Lancamento> lancamentosEncontrados = Lists.newArrayList(lancamentosIterable);
    
    assertEquals(1, lancamentosEncontrados.size());
    assertEquals(lancamento, lancamentosEncontrados.get(0));   
  }
}
